package br.com.adailton.cerqueira.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author adailton
 */
public interface ICalculadora extends Remote {
    
    public int adicao(int x, int y) throws RemoteException;
    public int subtracao(int x, int y) throws RemoteException;
    public int multiplicacao(int x, int y) throws RemoteException;
    public double divisao(int x, int y) throws RemoteException;
}

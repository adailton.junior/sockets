package br.com.adailton.cerqueira.rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author adailton
 */
public class ServidorRmi {

    public static void main(String[] args) {
        try {
            ICalculadora obj = new Calculadora();
            String objNome = "RemoteObject";

            // Registre o objeto remoto com o nome "RemoteObject"
            System.out.println("Registrando o objeto e iniciando o servidor...");
            Registry registro = LocateRegistry.getRegistry(1099);
            registro.rebind(objNome, obj);

            System.out.println("Servidor pronto...");
        } catch (Exception e) {
            System.err.println("Erro no servidor: " + e.toString());
            e.printStackTrace();
        }
    }
    
}

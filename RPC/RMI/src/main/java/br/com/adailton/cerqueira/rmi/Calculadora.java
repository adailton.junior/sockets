package br.com.adailton.cerqueira.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adailton
 */
public class Calculadora extends UnicastRemoteObject implements ICalculadora {
    
    private int MILISEGUNDO = 1000;
    
    public Calculadora() throws RemoteException {
        super();
    }

    public int adicao(int x, int y) throws RemoteException {
        System.out.println("Executando o método de adição!");
        return x + y;
    }

    public int subtracao(int x, int y) throws RemoteException {
        try {
            System.out.println("Executando o método de subtração!");
            Thread.sleep(20*MILISEGUNDO);
        } catch (InterruptedException ex) {
            Logger.getLogger(Calculadora.class.getName()).log(Level.SEVERE, null, ex);
        }
        return x - y;
    }

    public int multiplicacao(int x, int y) throws RemoteException {
        return x * y;
    }

    public double divisao(int x, int y) throws RemoteException {
        return x / y;
    }
}

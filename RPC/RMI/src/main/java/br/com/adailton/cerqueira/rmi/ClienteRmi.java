package br.com.adailton.cerqueira.rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
/**
 *
 * @author adailton
 */
public class ClienteRmi {
    
    private long startTime, endTime;

    public static void main(String[] args) {
        ClienteRmi cliente = new ClienteRmi();
        try {
            // Obtém o objeto remoto "RemoteObject" do registro de nomes
            String objNome = "RemoteObject";
            Registry registry = LocateRegistry.getRegistry();
            ICalculadora stub = (ICalculadora) registry.lookup(objNome);

            cliente.iniciarCronometro();
            // Invoca o método remoto de soma
            int response = stub.adicao(20, 20);
            System.out.println("Resposta do servidor: " + response);
            cliente.finalizarCronometro();
            
            cliente.iniciarCronometro();
            // Invoca o método remoto de subtração
            response = stub.subtracao(20, 20);
            System.out.println("Resposta do servidor: " + response);
            cliente.finalizarCronometro();
            
        } catch (Exception e) {
            System.err.println("Erro no cliente: " + e.toString());
            e.printStackTrace();
        }
    }

    public void iniciarCronometro() {
        this.startTime = System.currentTimeMillis();
    }
    
    public void finalizarCronometro() {
        this.endTime = System.currentTimeMillis();
        long duration = (this.endTime - this.startTime);  // em nanossegundos
        System.out.println("Tempo da resposta do servidor: " + duration + " ms");
    }
}

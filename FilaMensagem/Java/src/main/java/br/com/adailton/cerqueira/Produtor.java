package br.com.adailton.cerqueira;

import java.util.Scanner;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Produtor {
    public static void main(String[] args) throws Exception {
        String uri = "amqp://guest:guest@127.0.0.1";
        Scanner scan = new Scanner(System.in);
        boolean isFinish = true;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri);

        // Configuração recomendada
        factory.setConnectionTimeout(30000);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        String queue = "SistemaDistribuido";     // Nome da fila de mensagem
        boolean durable = true;    //durable - RabbitMQ nunca perderá a fila se uma falha ocorrer
        boolean exclusive = false;  //exclusive - A fila será usada apenas por uma conexão
        boolean autoDelete = false; //autodelete - A fila é excluída quando o último inscrito cancela a assinatura

        channel.queueDeclare(queue, durable, exclusive, autoDelete, null);

        String exchangeName = "SistemaDistribuido";
        String routingKey = "routing-key-sistema-distribuido";
        
        while(isFinish) {
            // Publica a mensagem no servidor RabbitMQ
            System.out.println("Digite sua mensagem:");
            String msg = scan.next();
            channel.basicPublish(exchangeName, routingKey, null, msg.getBytes());
        }

    }
}

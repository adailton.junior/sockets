package br.com.adailton.cerqueira;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

public class Consumidor {
    public static void main(String[] args) throws Exception {
        String uri = "amqp://guest:guest@127.0.0.1";

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri);

        // Configuração recomendada
        factory.setConnectionTimeout(30000);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        String queue = "SistemaDistribuido";     // Nome da fila de mensagem
        boolean durable = true;    //durable - RabbitMQ nunca perderá a fila se uma falha ocorrer
        boolean exclusive = false;  //exclusive - A fila será usada apenas por uma conexão
        boolean autoDelete = false; //autodelete - A fila é excluída quando o último inscrito cancela a assinatura

        channel.queueDeclare(queue, durable, exclusive, autoDelete, null);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String response = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + response + "'");
        };
        channel.basicConsume(queue, true, deliverCallback, consumerTag -> { });
    }
}

# Fila de Mensagem

Neste projeto apresentamos alguns exemplos de como utilizar o RabbitMQ com o Java e Spring Boot.

## Pré-requisito

- Java
- Docker
- IDE

## Instalação e configuração

Para os exemplos, utilizamos uma instalação local em um container docker. No site do RabbitMQ já tem o comando para baixar e executar o container (https://www.rabbitmq.com/download.html). Ex.: docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.12-management

Para verificar a instalação local do RabbitMQ é só acessar a url http://localhost:15672/ e autenticar com usuário guest e senha guest.

Dentro do RabbitMQ realizamos as seguintes configurações:
- Criamos uma Queue chamada "SistemaDistribuido";
- Criamos um Exchange chamado "SistemaDistribuido";
- No Exchange "SistemaDistribuido" atribuimos uma Routing Key chamada "routing-key-sistema-distribuido";
- Todas as demais configurações ficaram em default.


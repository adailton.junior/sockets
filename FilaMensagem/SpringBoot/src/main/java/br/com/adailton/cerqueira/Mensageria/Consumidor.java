package br.com.adailton.cerqueira.Mensageria;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class Consumidor {

    @RabbitListener(queues = {"${queue.name}"})
    public void receive(@Payload String msg) {
        System.out.println("Mensagem: " + msg);
    }
}

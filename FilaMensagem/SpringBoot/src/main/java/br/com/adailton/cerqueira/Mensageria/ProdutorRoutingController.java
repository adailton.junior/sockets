package br.com.adailton.cerqueira.Mensageria;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/produtor-routing")
public class ProdutorRoutingController {

    private final AmqpTemplate queue;

    public ProdutorRoutingController(AmqpTemplate queue) {
        this.queue = queue;

    }

    @GetMapping
    public String send() {
        this.queue.convertAndSend("SistemaDistribuido", "routing-key-sistema-distribuido", "mensagem de teste usando routing!");
        return "Mensagem de teste enviada!";
    }
}

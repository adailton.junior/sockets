package br.com.adailton.cerqueira.Mensageria;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/produtor")
public class ProdutorController {

    @Autowired
    private PublicadorQueue publicador;

    @GetMapping
    public String send() {
        this.publicador.send("mensagem de teste!");
        return "Mensagem de teste enviada!";
    }
}

package br.com.adailton.cerqueira.Mensageria;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PublicadorQueue {
    
    @Autowired
    private RabbitTemplate rabbit;

    @Autowired
    private Queue queue;

    public void send(String order) {
        this.rabbit.convertAndSend(this.queue.getName(), order);
    }
}

package br.com.adailton.cerqueira.Mensageria;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PublicadorConfig {
    
    @Value("${queue.name}")
    private String mensagem;

    @Bean
    public Queue queue() {
        return new Queue(this.mensagem, true);
    }
}

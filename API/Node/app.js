const express = require('express');
const consign = require('consign');
const Tabelas = require('./config/Tabelas');

Tabelas.init();
Tabelas.seed();

const app = express();
const PORT = 3000;

consign().include('controllers').into(app);

app.get('/', (request, response) => {
    response.send('Servidor on-line!')
})

app.get('/', (request, response) => {
    response.json({ message: 'API on-line!' });
});

app.listen(PORT, () => {
    console.log(`Servidor executando na porta ${PORT}`);
});




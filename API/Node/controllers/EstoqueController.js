const EstoqueDAO = require('../dao/EstoqueDao')

module.exports = app => {
    app.get('/estoques', (request, response) => {
        EstoqueDAO.all((err, estoques) => {
            response.header("Access-Control-Allow-Origin", "*");
            if (err == null) {
                response.send(estoques);
            } else {
                response.status(404).send('Not found');
            }
        });

    })
}



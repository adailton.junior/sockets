const ClienteDAO = require('../dao/ClienteDao')

module.exports = app => {
    app.get('/clientes', (request, response) => {
        ClienteDAO.all((err, clientes) => {
            response.header("Access-Control-Allow-Origin", "*");
            if (err == null) {
                response.send(clientes);
            } else {
                response.status(404).send('Not found');
            }
        });

    })

    app.post('/clientes', (request, response) => {
        console.log(request.body);
        response.json({ message: 'API on-line!' });
    })
}



package br.com.adailton.cerqueira.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author adailton
 */
public class ServidorSocket {
    public static void main(String[] args) {
        try {
            // Cria um socket servidor na porta 1234
            ServerSocket serverSocket = new ServerSocket(1234);
            System.out.println("Servidor iniciado. Aguardando conexões...");
            while (true) {
                // Aguarda conexões do cliente
                Socket socket = serverSocket.accept();
                // Cria uma nova thread para lidar com o cliente
                Thread thread = new Thread(new ClienteHandler(socket));
                // Inicia a thread
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

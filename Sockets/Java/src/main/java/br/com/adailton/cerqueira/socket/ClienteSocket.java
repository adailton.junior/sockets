package br.com.adailton.cerqueira.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author adailton
 */
public class ClienteSocket {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("localhost", 1234);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            Scanner scan = new Scanner(System.in);
            boolean isFinish = true;

            while(isFinish) {
                // envia uma mensagem para o servidor
                System.out.println("Digite sua mensagem:");
                String msg = scan.next();
                out.println(msg);

                // lê a resposta do servidor
                String resposta = in.readLine();
                System.out.println("Mensagem do servidor: " + resposta);
                
                if (msg.toUpperCase().equals("BYE")) {
                    isFinish = false;
                }
            }

            // fecha o socket e os streams
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

import socket
import threading

HOST = socket.gethostname(); # endereço IP do servidor
PORT = 5000; # porta do servidor

def handle_client(conn, addr):
    print(f'Nova conexão estabelecida com {addr}');
    while True:
        data = conn.recv(1024).decode();
        
        if not data:
            break;
        print("Mensagem do usuário: " + str(data));
        data = input('Digite sua mensagem: ');
        conn.send(data.encode());
    # Fecha a conexão com o cliente
    conn.close();

def server():
    serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
    serverSocket.bind((HOST, PORT));
    print("Servidor iniciado em " + str(HOST) + ":" + str(PORT));
    
    serverSocket.listen(5);
    
    while True:
        conn, address = serverSocket.accept();
        print("Conexão para: " + str(address));
        client_thread = threading.Thread(target=handle_client, args=(conn, address));
        client_thread.start();    
    
if __name__ == '__main__':
    server();
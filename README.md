# Sistemas Distribuidos

Projeto criado para armazenar os código com exemplos dos laboratórios de sistemas distribuídos

## Sockets

Código com exemplos de conexão em sockets para aula de Sistemas Distribuídos. São apresentados códigos em Java e Python.

## RPC

Código com exemplo da utilização de RMI (Remote Method Invocation) com Java.

## Fila de Mensagem

Código com exemplos para utilizar a fila de mensagem RabbitMQ com Java e Spring Boot.

## API

EM CONSTRUÇÃO...

## Eleição

Código em Java para exemplificar um algoritmo simples de eleição.

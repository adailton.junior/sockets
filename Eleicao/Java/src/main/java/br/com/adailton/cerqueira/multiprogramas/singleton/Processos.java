package br.com.adailton.cerqueira.multiprogramas.singleton;

import br.com.adailton.cerqueira.multiprogramas.model.Processo;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author adailton
 */
public class Processos {
    private static Processos uniqueInstance;
    
    private Map<Integer, Processo> processos;
    
    private Processo me;
    private Processo lider;
    
    private Integer totalProcesso;

    private Processos() {
    }

    public static synchronized Processos getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Processos();
        }
        return uniqueInstance;
    }
    
    public void config(Map<Integer, Processo> processos, Processo me, Processo lider, Integer totalProcesso) {
        this.processos = processos;
        this.me = me;
        this.lider = lider;
        this.totalProcesso = totalProcesso;
    }
    
    public Processo getRandomProcesso() {
        Random rand = new Random();
        int index = rand.nextInt(processos.size());
        index = (index == 0) ? processos.size() : index;
        return processos.get(index);
    }

    public Map<Integer, Processo> getProcessos() {
        return processos;
    }

    public Processo getMe() {
        return me;
    }
    
    public Processo getLider() {
        return lider;
    }
    
    public void setLider(Processo lider) {
        this.lider = lider;
    }

    public Integer getTotalProcesso() {
        return totalProcesso;
    }
}

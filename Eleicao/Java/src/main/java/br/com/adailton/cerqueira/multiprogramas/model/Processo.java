package br.com.adailton.cerqueira.multiprogramas.model;

/**
 *
 * @author adailton
 */
public class Processo {
    private Integer identificador;
    private String host;
    private Integer port;
    private Boolean isLider;
    private Boolean isDown;

    public Processo(Integer identificador, String host, Integer port) {
        this.identificador = identificador;
        this.host = host;
        this.port = port;
        this.isLider = false;
    }

    public Integer getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador = identificador;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean isLider() {
        return isLider;
    }

    public void setIsLider(Boolean isLider) {
        this.isLider = isLider;
    }
    
    public Boolean isDown() {
        return isDown;
    }

    public void setIsDown(Boolean isDown) {
        this.isDown = isDown;
    }
}

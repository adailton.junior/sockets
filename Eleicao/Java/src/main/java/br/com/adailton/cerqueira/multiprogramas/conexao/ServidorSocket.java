package br.com.adailton.cerqueira.multiprogramas.conexao;

import br.com.adailton.cerqueira.multiprogramas.model.Processo;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author adailton
 */
public class ServidorSocket implements Runnable {
    
    private ServerSocket serverSocket;
    private Processo me;
            
    public ServidorSocket(Processo me, Integer port) throws IOException {
        this.me = me;
        serverSocket = new ServerSocket(port);
        System.out.println("Servidor iniciado na porta " + port + ". Aguardando conexões...");
    }
    
    @Override
    public void run() {
        try {            
            while (true) {
                // Aguarda conexões do cliente
                Socket socket = serverSocket.accept();
                // Cria uma nova thread para lidar com o cliente
                Thread thread = new Thread(new ClienteHandler(this.me, socket));
                // Inicia a thread
                thread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

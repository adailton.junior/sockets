package br.com.adailton.cerqueira.multiprogramas;

import br.com.adailton.cerqueira.multiprogramas.tipo.Consumidor;
import br.com.adailton.cerqueira.multiprogramas.tipo.Produtor;

/**
 *
 * @author adailton
 */
public class MultiPrograma {

    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Para executar o programa: MultiPrograma <tipo> <identificador> <porta>");
            System.exit(0);
        }
        
        String tipo = args[0];
        String nome = args[1];
        Integer porta = Integer.parseInt(args[2]);
        System.out.println("Olá, eu sou o programa do tipo " + tipo + " com o identificador " + nome);
        
        if (tipo.equals("produtor")) {
            Produtor p = new Produtor(porta, nome);
            p.run();
        } else if (tipo.equals("consumidor")) {
            Consumidor c = new Consumidor(porta, nome);
            c.run();
        } else {
            System.out.println("Tipo não valido!");
        }
    }
}
